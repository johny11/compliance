class ParkingUrban{
  String name;
  String radioValue;
  int check;


  ParkingUrban({
    this.name,
    this.radioValue,
    this.check

  });


  factory ParkingUrban.fromJson(Map<String, dynamic> parsedJson){
    return ParkingUrban(

        name : parsedJson['name'],
        radioValue : parsedJson ['radioValue'],
        check : parsedJson ['check']
    );
  }



 set radioButtonValue(String  value) {
    radioValue = value;
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'radioValue': radioValue,
        'check':check
      };

}
import 'package:flutter/material.dart';
import 'package:compliance/screens/testform.dart';

import 'package:compliance/modals/placing_vehicle_lights.dart';
import 'package:compliance/modals/vehicle_coupling.dart';
import 'package:compliance/screens/placing_motion.dart';




import 'dart:convert';


class CouplingForm extends StatefulWidget {
  CouplingForm({Key key}) : super(key: key);



  @override
  _CouplingFormState createState() => _CouplingFormState();
}

class _CouplingFormState extends State<CouplingForm> {


  List<Coupling> couplingList =[];


  @override
  void initState() {
    super.initState();
    this.loadPreForm();

  }



  loadPreForm() async {
    String data = await DefaultAssetBundle
        .of(context)
        .loadString('data_repo/coupling.json');
    final jsonResult = await json.decode(data);

    couplingList= jsonResult.map<Coupling>((i)=>Coupling.fromJson(i)).toList();

    setState(() {

    });

  }


  void valueSlected(int value,Coupling coupling){

    switch (value) {
      case 0:
        coupling.radioButtonValue ="checked";
        coupling.check = 0;
        //Fluttertoast.showToast(msg:  placingform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
      case 1:
        coupling.radioButtonValue ="Repair";
        coupling.check = 1;
       // Fluttertoast.showToast(msg: placingform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
      case 2:
        coupling.radioButtonValue="N/a";
        coupling.check = 2;
        //Fluttertoast.showToast(msg: placingform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
    }


  }

  Widget listItem(int index,Coupling coupling,int length){
    int  vv =  index+1;
    return(new Card(

      child: new Container(
        // color:Colors.green,
        padding: new EdgeInsets.symmetric(vertical:20,horizontal:15),

        child:new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,

          children: <Widget>[
            new Text( '$vv/'+length.toString(),textAlign: TextAlign.end,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            new SizedBox(height: 3,),
            new Text( coupling.name,textAlign: TextAlign.start,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            new SizedBox(height: 5,),
            new Row(
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                new Radio(
                    value: 0,
                    groupValue: coupling.check,
                    onChanged: (value){

                      valueSlected(value,coupling);
//
//                                  preformList.removeWhere((item) => item == preformList[index]);
//                                    setState(() {
//
//                                    });



                    },
                    activeColor:Colors.green
                ),
                new Text(
                  'Checked',
                  style: new TextStyle(fontSize: 17.0),
                ),
                new Radio(
                    value: 1,
                    groupValue:   coupling.check,
                    onChanged: (value){
                      valueSlected(value,coupling);

                    },
                    activeColor:Colors.red
                ),
                new Text(
                  'Repair',
                  style: new TextStyle(
                    fontSize: 17.0,
                  ),
                ),
                new Radio(
                    value: 2,
                    groupValue:   coupling.check,
                    onChanged: (value){
                      valueSlected(value,coupling);

                    },
                    activeColor:Colors.yellow
                ),
                new Text(
                  'N/A',
                  style: new TextStyle(fontSize: 17.0),
                ),
              ],
            ),
          ],
        ),


      ),



    )




    );
  }



  void _Next() {

    Navigator.pushNamed(context, '/part_4_Backing');


  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
       // title: Text("PART 2: PLACING THE VEHICLE IN MOTION",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
        centerTitle: true,
        backgroundColor: const Color(0xffbF1F1F1),
        elevation: 0.0,
      ),
      body: new Container(color:  const Color(0xffbF1F1F1),
        padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text("PART 3",textAlign: TextAlign.end,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            SizedBox(height: 5,),
            Text("COUPLING AND UNCOUPLING",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 18)),
            SizedBox(height: 10,),
          //  Padding(padding: EdgeInsets.symmetric(vertical: 5.0,horizontal: 0.0),child:  Text("Steering",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 18)),),

            Expanded(child: new ListView.builder(
              // Build the ListView
              itemBuilder: (BuildContext context, int index) {
                return listItem(index,couplingList[index],couplingList.length);
              },
              itemCount: couplingList == null ? 0 : couplingList.length,
            ))],),


      )
      ,

      floatingActionButton: FloatingActionButton(
        backgroundColor:Colors.green,
        onPressed: _Next,
        tooltip: 'Next',
        child: Icon(Icons.arrow_right),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class Lights{
  String name;
  String radioValue;
  int check;


  Lights({
    this.name,
    this.radioValue,
    this.check

  });


  factory Lights.fromJson(Map<String, dynamic> parsedJson){
    return Lights(

        name : parsedJson['name'],
        radioValue : parsedJson ['radioValue'],
        check : parsedJson ['check']
    );
  }



 set radioButtonValue(String  value) {
    radioValue = value;
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'radioValue': radioValue,
        'check':check
      };

}
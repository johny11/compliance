class ParkingRural{
  String name;
  String radioValue;
  int check;


  ParkingRural({
    this.name,
    this.radioValue,
    this.check

  });


  factory ParkingRural.fromJson(Map<String, dynamic> parsedJson){
    return ParkingRural(

        name : parsedJson['name'],
        radioValue : parsedJson ['radioValue'],
        check : parsedJson ['check']
    );
  }



 set radioButtonValue(String  value) {
    radioValue = value;
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'radioValue': radioValue,
        'check':check
      };

}
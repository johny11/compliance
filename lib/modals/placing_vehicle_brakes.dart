class Brakes{
  String name;
  String radioValue;
  int check;


  Brakes({
    this.name,
    this.radioValue,
    this.check

  });


  factory Brakes.fromJson(Map<String, dynamic> parsedJson){
    return Brakes(

        name : parsedJson['name'],
        radioValue : parsedJson ['radioValue'],
        check : parsedJson ['check']
    );
  }



 set radioButtonValue(String  value) {
    radioValue = value;
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'radioValue': radioValue,
        'check':check
      };

}
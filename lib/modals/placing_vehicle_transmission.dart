class Transmission{
  String name;
  String radioValue;
  int check;


  Transmission({
    this.name,
    this.radioValue,
    this.check

  });


  factory Transmission.fromJson(Map<String, dynamic> parsedJson){
    return Transmission(

        name : parsedJson['name'],
        radioValue : parsedJson ['radioValue'],
        check : parsedJson ['check']
    );
  }



 set radioButtonValue(String  value) {
    radioValue = value;
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'radioValue': radioValue,
        'check':check
      };

}
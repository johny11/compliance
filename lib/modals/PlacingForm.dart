class PlacingForm{
  String name;
  String radioValue;
  int check;


  PlacingForm({
    this.name,
    this.radioValue,
    this.check

  });


  factory PlacingForm.fromJson(Map<String, dynamic> parsedJson){
    return PlacingForm(

        name : parsedJson['name'],
        radioValue : parsedJson ['radioValue'],
        check : parsedJson ['check']
    );
  }



 set radioButtonValue(String  value) {
    radioValue = value;
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'radioValue': radioValue,
        'check':check
      };

}
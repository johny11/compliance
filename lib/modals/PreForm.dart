class PreForm{
  String name;
  String radioValue;
  int check;


  PreForm({
    this.name,
    this.radioValue,
    this.check

  });


  factory PreForm.fromJson(Map<String, dynamic> parsedJson){
    return PreForm(

        name : parsedJson['name'],
        radioValue : parsedJson ['radioValue'],
        check : parsedJson ['check']
    );
  }



 set radioButtonValue(String  value) {
    radioValue = value;
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'radioValue': radioValue,
        'check':check
      };

}
import 'package:flutter/material.dart';
import 'package:compliance/screens/testform.dart';

import 'package:compliance/modals/PreForm.dart';
import 'package:compliance/screens/placing_motion.dart';




import 'dart:convert';


class PretripForm extends StatefulWidget {
  PretripForm({Key key}) : super(key: key);



  @override
  _PretripFormState createState() => _PretripFormState();
}

class _PretripFormState extends State<PretripForm> {


  List<PreForm> preformList =[];


  @override
  void initState() {
    super.initState();
    this.loadPreForm();
  
  }



   loadPreForm() async {
    String data = await DefaultAssetBundle
        .of(context)
        .loadString('data_repo/preform_data.json');
    final jsonResult = await json.decode(data);

    preformList= jsonResult.map<PreForm>((i)=>PreForm.fromJson(i)).toList();

        setState(() {

        });

  }

  void valueSlected(int value,PreForm preform){

    switch (value) {
      case 0:
        preform.radioButtonValue ="checked";
        preform.check = 0;
      //  Fluttertoast.showToast(msg:  preform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
      case 1:
        preform.radioButtonValue ="Repair";
        preform.check = 1;
      //  Fluttertoast.showToast(msg: preform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
      case 2:
        preform.radioButtonValue="N/a";
        preform.check = 2;
       // Fluttertoast.showToast(msg: preform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
    }


  }

  Widget listItem(int index,PreForm preform,int length){
    int  vv =  index+1;

    return(new Card(

      child: new Container(
        // color:Colors.green,
        padding: new EdgeInsets.symmetric(vertical:20,horizontal:15),

        child:new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,

          children: <Widget>[
            new Text( '$vv/'+length.toString(),textAlign: TextAlign.end,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            new SizedBox(height: 3,),
            new Text( preform.name,textAlign: TextAlign.start,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            new SizedBox(height: 5,),
            new Row(
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                new Radio(
                    value: 0,
                    groupValue: preform.check,
                    onChanged: (value){

                      valueSlected(value,preform);
//
//                                  preformList.removeWhere((item) => item == preformList[index]);
//                                    setState(() {
//
//                                    });



                    },
                    activeColor:Colors.green
                ),
                new Text(
                  'Checked',
                  style: new TextStyle(fontSize: 17.0),
                ),
                new Radio(
                    value: 1,
                    groupValue:   preform.check,
                    onChanged: (value){
                      valueSlected(value,preform);

                    },
                    activeColor:Colors.red
                ),
                new Text(
                  'Repair',
                  style: new TextStyle(
                    fontSize: 17.0,
                  ),
                ),
                new Radio(
                    value: 2,
                    groupValue:   preform.check,
                    onChanged: (value){
                      valueSlected(value,preform);

                    },
                    activeColor:Colors.yellow
                ),
                new Text(
                  'N/A',
                  style: new TextStyle(fontSize: 17.0),
                ),
              ],
            ),

          ],
        ),


      ),



    )




    );
  }






  void _Next() {

    Navigator.pushNamed(context, '/part_2_motor');


  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
       // title: Text("PART 1: PRE-TRIP INSPECTION",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
        centerTitle: true,
        backgroundColor: const Color(0xffbF1F1F1),
        elevation: 0.0,
      ),
       body: new Container(
        padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 14.0),
        color:  const Color(0xffbF1F1F1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text("PART 1",textAlign: TextAlign.end,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            SizedBox(height: 5,),
          Text("PRE-TRIP INSPECTION",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 18)),
          SizedBox(height: 10,),
          Expanded(child:
               new ListView.builder(

                    itemCount: preformList == null ? 0 : preformList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return listItem(index,preformList[index],preformList.length);
                    },

                ),)

],),

//        child: new Center(
//
//
//          child:
//
//                new ListView.builder(
//                    itemCount: preformList == null ? 0 : preformList.length,
//                    itemBuilder: (BuildContext context, int index) {
//                      return listItem(index,preformList[index],preformList.length);
//                    },
//
//                ),
//
//        ),
      )
      ,
          
      floatingActionButton: FloatingActionButton(
        backgroundColor:Colors.green,
        onPressed: _Next,
        tooltip: 'Next',
        child: Icon(Icons.arrow_right),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

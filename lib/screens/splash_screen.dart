import 'package:flutter/material.dart';
import 'dart:async';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }
  @override
  void initState() {
    super.initState();
    startTime();
  }
  void navigationPage() {
    Navigator.of(context).pushReplacementNamed('/vehicle');
  }


  @override
  Widget build(BuildContext context) {
    return new Stack(
        fit: StackFit.expand,

        children: <Widget> [
          new Image.asset('images/bgg.jpg', fit: BoxFit.fill,),]

    );
  }
}
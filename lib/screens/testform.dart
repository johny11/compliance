import 'package:flutter/material.dart';

import 'package:compliance/screens/pretrip_form.dart';
class DriverForm extends StatefulWidget {
  DriverForm({Key key}) : super(key: key);



  @override
  _DriverFormState createState() => _DriverFormState();
}

class _DriverFormState extends State<DriverForm> {


  void part_one() {

    Navigator.pushNamed(context, '/part_1_pretrip');
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
       // title: Text(" MOTION",textAlign: TextAlign.end,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
        //centerTitle: true,
        backgroundColor: const Color(0xffbF1F1F1),
        elevation: 0.0,
      ),

      body:  new Container(
          padding: new EdgeInsets.symmetric(horizontal:30.0),
          color: const Color(0xffbF1F1F1),
          height: MediaQuery.of(context).size.height,
          child:
          SingleChildScrollView(

            child:Column(
           crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
//              Text("Form",textAlign: TextAlign.end,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
//             SizedBox(height: 10,),
              Text("Driver's road test",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
              SizedBox(height: 15,),
              Card(
              elevation: 5.0,

              child:


              Container(

                padding: new EdgeInsets.all(10.0),
                child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,

                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Driver's name:"
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Address:"
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "License:"
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Equipment driven:"
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Test date:"
                    ),
                  ),

                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Starting miles:"
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Ending miles: "
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Miles driven:"
                    ),
                  ),

                  SizedBox(height: 15,),
                ],
              ),),
           )

          ],),

          )




      ,
      ),



      floatingActionButton: FloatingActionButton(
      backgroundColor:Colors.green,
      onPressed: part_one,
      tooltip: 'Next',
      child: Icon(Icons.arrow_right),
    ),

      );


  }
}

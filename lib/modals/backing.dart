class Backing{
  String name;
  String radioValue;
  int check;


  Backing({
    this.name,
    this.radioValue,
    this.check

  });


  factory Backing.fromJson(Map<String, dynamic> parsedJson){
    return Backing(

        name : parsedJson['name'],
        radioValue : parsedJson ['radioValue'],
        check : parsedJson ['check']
    );
  }



 set radioButtonValue(String  value) {
    radioValue = value;
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'radioValue': radioValue,
        'check':check
      };

}
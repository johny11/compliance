import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:compliance/screens/pretrip_form.dart';
import 'package:compliance/screens/vehicle.dart';
import 'package:compliance/screens/placing_motion.dart';
import 'package:compliance/screens/testform.dart';

import 'package:compliance/screens/splash_screen.dart';
import 'package:compliance/screens/placing_vehicleBrakes.dart';
import 'package:compliance/screens/placing_vehicleLights.dart';
import 'package:compliance/screens/placing_vehicleTransmission.dart';
import 'package:compliance/screens/placing_vehicleSteering.dart';
import 'package:compliance/screens/coupling_uncoupling.dart';
import 'package:compliance/screens/vehicle_backing.dart';
import 'package:compliance/screens/vehicle_parking_rural.dart';
import 'package:compliance/screens/vehicle_parking_urban.dart';



void main() {
  runApp(new MaterialApp(

      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/': (BuildContext context) => SplashScreen(),
        '/vehicle': (BuildContext context) => Vehicle(),
        '/driver_form': (BuildContext context) => DriverForm(),
        '/part_1_pretrip': (BuildContext context) => PretripForm(),
        '/part_2_motor': (BuildContext context) => PlacingMotionForm(),
        '/part_2_transmission': (BuildContext context) => TransmissionForm(),
        '/part_2_brakes': (BuildContext context) => BrakesForm(),
        '/part_2_lights': (BuildContext context) => LightsForm(),
        '/part_2_Steering': (BuildContext context) => SteeringForm(),
        '/part_3_Coupling': (BuildContext context) => CouplingForm(),
        '/part_4_Backing': (BuildContext context) => BackingForm(),
        '/part_4_ParkingUrban': (BuildContext context) => ParkingUrbanForm(),
        '/part_4_ParkingRural': (BuildContext context) => ParkingRuralForm(),
      }
  ));
}

//class MyApp extends StatefulWidget {
//  @override
//  MyAppState createState() => new MyAppState();
//}
//
//class MyAppState extends State<MyApp> {
// // List data;
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//        appBar: new AppBar(
//          title: new Text("Load local JSON file"),
//        ),
//        body: new Container(
//          child: new Center(
//            // Use future builder and DefaultAssetBundle to load the local JSON file
//            child: new FutureBuilder(
//                future: DefaultAssetBundle
//                    .of(context)
//                    .loadString('data_repo/starwars_data.json'),
//                builder: (context, snapshot) {
//                  // Decode the JSON
//                  var new_data = json.decode(snapshot.data.toString());
//
//                  return new ListView.builder(
//                    // Build the ListView
//                    itemBuilder: (BuildContext context, int index) {
//                      return new Card(
//                        child: new Column(
//                          crossAxisAlignment: CrossAxisAlignment.stretch,
//                          children: <Widget>[
//                            new Text("Name: " + new_data[index]['name']),
//                            new Text("Height: " + new_data[index]['height']),
//                            new Text("Mass: " + new_data[index]['mass']),
//                            new Text(
//                                "Hair Color: " + new_data[index]['hair_color']),
//                            new Text(
//                                "Skin Color: " + new_data[index]['skin_color']),
//                            new Text(
//                                "Eye Color: " + new_data[index]['eye_color']),
//                            new Text(
//                                "Birth Year: " + new_data[index]['birth_year']),
//                            new Text("Gender: " + new_data[index]['gender'])
//                          ],
//                        ),
//                      );
//                    },
//                    itemCount: new_data == null ? 0 : new_data.length,
//                  );
//                }),
//          ),
//        ));
//  }
//}
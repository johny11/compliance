import 'package:flutter/material.dart';
import 'package:compliance/screens/testform.dart';

import 'package:compliance/modals/placing_vehicle_lights.dart';
import 'package:compliance/modals/parking_rural.dart';
import 'package:compliance/screens/placing_motion.dart';




import 'dart:convert';


class ParkingRuralForm extends StatefulWidget {
  ParkingRuralForm({Key key}) : super(key: key);



  @override
  _ParkingRuralFormState createState() => _ParkingRuralFormState();
}

class _ParkingRuralFormState extends State<ParkingRuralForm> {


  List<ParkingRural> parkingRuralList =[];


  @override
  void initState() {
    super.initState();
    this.loadPreForm();

  }



  loadPreForm() async {
    String data = await DefaultAssetBundle
        .of(context)
        .loadString('data_repo/parking_rural.json');
    final jsonResult = await json.decode(data);

    parkingRuralList= jsonResult.map<ParkingRural>((i)=>ParkingRural.fromJson(i)).toList();

    setState(() {

    });

  }


  void valueSlected(int value,ParkingRural parkingRural){

    switch (value) {
      case 0:
        parkingRural.radioButtonValue ="checked";
        parkingRural.check = 0;
        //Fluttertoast.showToast(msg:  placingform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
      case 1:
        parkingRural.radioButtonValue ="Repair";
        parkingRural.check = 1;
       // Fluttertoast.showToast(msg: placingform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
      case 2:
        parkingRural.radioButtonValue="N/a";
        parkingRural.check = 2;
        //Fluttertoast.showToast(msg: placingform.radioValue,toastLength: Toast.LENGTH_SHORT);
        setState(() {

        });
        break;
    }


  }

  Widget listItem(int index,ParkingRural parkingRural,int length){

    int  vv =  index+1;
    return(new Card(

      child: new Container(
        // color:Colors.green,
        padding: new EdgeInsets.symmetric(vertical:20,horizontal:15),

        child:new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,

          children: <Widget>[
            new Text( '$vv/'+length.toString(),textAlign: TextAlign.end,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            new SizedBox(height: 3,),
            new Text( parkingRural.name,textAlign: TextAlign.start,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            new SizedBox(height: 5,),
            new Row(
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                new Radio(
                    value: 0,
                    groupValue: parkingRural.check,
                    onChanged: (value){

                      valueSlected(value,parkingRural);
//
//                                  preformList.removeWhere((item) => item == preformList[index]);
//                                    setState(() {
//
//                                    });



                    },
                    activeColor:Colors.green
                ),
                new Text(
                  'Checked',
                  style: new TextStyle(fontSize: 17.0),
                ),
                new Radio(
                    value: 1,
                    groupValue:   parkingRural.check,
                    onChanged: (value){
                      valueSlected(value,parkingRural);

                    },
                    activeColor:Colors.red
                ),
                new Text(
                  'Repair',
                  style: new TextStyle(
                    fontSize: 17.0,
                  ),
                ),
                new Radio(
                    value: 2,
                    groupValue:   parkingRural.check,
                    onChanged: (value){
                      valueSlected(value,parkingRural);

                    },
                    activeColor:Colors.yellow
                ),
                new Text(
                  'N/A',
                  style: new TextStyle(fontSize: 17.0),
                ),
              ],
            ),
          ],
        ),


      ),



    )




    );
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Driver Checklist Form"),
          content: new Text("Your form submitted successfully"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(

              child: new Text("Ok",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.green)),
              onPressed: () {
                Navigator.popUntil(context, ModalRoute.withName('/vehicle'));
              },
            ),
          ],
        );
      },
    );
  }

  void _Next() {
    this._showDialog();
   // Navigator.popUntil(context, ModalRoute.withName('/vehicle'));

    //Navigator.pushNamed(context, '/part_3_Coupling');


  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
       // title: Text("PART 2: PLACING THE VEHICLE IN MOTION",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
        centerTitle: true,
        backgroundColor: const Color(0xffbF1F1F1),
        elevation: 0.0,
      ),
      body: new Container(color:  const Color(0xffbF1F1F1),
        padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text("PART 4",textAlign: TextAlign.end,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
            SizedBox(height: 5,),
            Text("BACKING AND PARKING",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 18)),

            Padding(padding: EdgeInsets.symmetric(vertical: 5.0,horizontal: 0.0),child:  Text("Parking (rural)",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 18)),),

            Expanded(child: new ListView.builder(
              // Build the ListView
              itemBuilder: (BuildContext context, int index) {
                return listItem(index,parkingRuralList[index],parkingRuralList.length);
              },
              itemCount: parkingRuralList == null ? 0 : parkingRuralList.length,
            ))],),


      )
      ,

      floatingActionButton: FloatingActionButton.extended(
        backgroundColor:Colors.green,
        onPressed: _Next,
        tooltip: 'Save',
        icon: Icon(Icons.save),
        label: Text("Save"),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

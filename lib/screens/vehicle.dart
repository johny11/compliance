import 'package:flutter/material.dart';

import 'dart:ui';

class Vehicle extends StatefulWidget {
  @override
  VehicleState createState() => new VehicleState();
}

class VehicleState extends State<Vehicle> {

  String _vehicle = 'Select Vehicle';
  next(){
    Navigator.pushNamed(context, '/driver_form');

  }
 // List data;

  @override
  Widget build(BuildContext context) {
    return

      Stack(
          fit: StackFit.expand,

          children: <Widget> [
            new Image.asset('images/bgg.jpg', fit: BoxFit.fill,),
            new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 0.8, sigmaY: 0.8),
              child: new Container(
                decoration: new BoxDecoration(color: Colors.black12.withOpacity(0.5)),
              ),
            ),
            new Scaffold(
                  backgroundColor: Colors.transparent,
                  appBar: AppBar(

                      title: Text("SELECT VEHICLE",style:TextStyle(fontSize: 16,color: Colors.white)),
                      centerTitle: true,
                       elevation: 0.0,
                       backgroundColor: const Color(0xFFB4C56C).withOpacity(0.0),
                      actions: <Widget>[
                      IconButton(
                      icon: Image.asset('images/user.png'),

                      tooltip: 'User'
                      )
                   ]

                    ),
                  body:Container(

                    padding: EdgeInsets.all(15),
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      
                      new Expanded(child: Center(
                        child:Container(padding: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
                            decoration: BoxDecoration(border: Border.all(color: Colors.white)),
                            child:
                        Theme(
                            data: Theme.of(context).copyWith(brightness: Brightness.dark),
                            child: DropdownButtonHideUnderline(child:  new DropdownButton<String>(

                              // style: TextStyle(color: Color(value)),
                              isDense: true,
                              hint:new Text(_vehicle,style:TextStyle(fontSize: 16,color: Colors.white)),
                              items: <String>['Vehicle 1', 'Vehicle 2','Vehicle 3','Vehicle 4'].map((String v) {
                                return new DropdownMenuItem<String>(
                                  value: v,
                                  child: new Text(v),
                                );
                              }).toList(),
                              onChanged: (String dv) {
                                this.setState(() {
                                  _vehicle = dv;
                                });
                                print(dv);
                              },))



                          //child: Text("Vehicle",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.white)),
                        ))





                      ),),



                      new RaisedButton(

                        padding: const EdgeInsets.all(8.0),
                        textColor: Colors.white,
                        color: Colors.green,
                        onPressed: next,
                        elevation: 5,
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                        child: new Text("NEXT",style:TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.white)),

                      ),
                    ],))

                ),


          ]



    );
  }
}